# TP2 - BLAS (Bibliothèque d’Algèbre Linéaire) et OpenMP

* **[Sujet](./docs/TP2MN.pdf)**

* **[Compte-Rendu](./docs/CR_MN_TP2.pdf)**

    * [Tableur des mesures](./docs/measures.ods)

* [Archive des squelettes des codes](./archive)

## Fonctions BLAS Implémentées:

### BLAS 1 (pour les opérations de type **vecteur-vecteur**)

| Fonctions BLAS 1 | Tests |
|:-----------------|:------|
| [swap.c](./src/swap.c) | [test_swap.c](./examples/test_swap.c) |
| [copy.c](./src/copy.c) | [test_copy.c](./examples/test_copy.c) |
| [dot.c](./src/dot.c) | [test_dot.c](./examples/test_dot.c) |
| [axpy.c](./src/axpy.c) | [test_axpy.c](./examples/test_axpy.c) |
| [asum.c](./src/asum.c) | [test_asum.c](./examples/test_asum.c) |
| [iamax.c](./src/iamax.c) | [test_iamax.c](./examples/test_iamax.c) |
| [iamin.c](./src/iamin.c) | [test_iamin.c](./examples/test_iamin.c) |
| [nrm2.c](./src/nrm2.c) | [test_nrm2.c](./examples/test_nrm2.c) |

#### Avec des directives OpenMP

| Fonctions BLAS 1 | Tests |
|:-----------------|:------|
| [copyOpenMP.c](./src/copyOpenMP.c) | [test_copyOpenMP.c](./examples/test_copyOpenMP.c) |
| [dotOpenMP.c](./src/dotOpenMP.c) | [test_dotOpenMP.c](./examples/test_dotOpenMP.c) |
| [axpyOpenMP.c](./src/axpyOpenMP.c) | [test_axpyOpenMP.c](./examples/test_axpyOpenMP.c) |

### BLAS 2 (pour les opérations de type **vecteur-matrice**)

| Fonctions BLAS 2 | Tests |
|:-----------------|:------|
| [gemv.c](./src/gemv.c) | [test_gemv.c](./examples/test_gemv.c) |

#### Avec des directives OpenMP

| Fonctions BLAS 2 | Tests |
|:-----------------|:------|
| [gemvOpenMP.c](./src/gemvOpenMP.c) | [test_gemvOpenMP.c](./examples/test_gemvOpenMP.c) |

### BLAS 3 (pour les opérations de type **matrice-matrice**)

| Fonctions BLAS 3 | Tests |
|:-----------------|:------|
| [gemm.c](./src/gemm.c) | [test_gemm.c](./examples/test_gemm.c) |

#### Avec des directives OpenMP

| Fonctions BLAS 3 | Tests |
|:-----------------|:------|
| [gemmOpenMP.c](./src/gemmOpenMP.c) | [test_gemmOpenMP.c](./examples/test_gemmOpenMP.c) |

***
