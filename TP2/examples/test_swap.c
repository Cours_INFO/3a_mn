#include <stdio.h>

#include "mnblas.h"
#include "complexe.h"

#include "flop.h"

#define VECSIZE    65536

#define NB_FOIS    10

typedef float vfloat [VECSIZE] ;
typedef double vdouble [VECSIZE] ;

typedef complexe_float_t vcomplexe [VECSIZE] ;
typedef complexe_double_t vcomplexedouble [VECSIZE] ;

vfloat vec1, vec2 ;
vdouble vec1d,vec2d;
vcomplexe vec3, vec4 ;
vcomplexedouble vec3d,vec4d;

void vector_init (vfloat V, double x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_init_double (vdouble V, double x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}
void vector_complexe_init (vcomplexe V, float a, float b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_complexe_init_double (vcomplexedouble V, double a, double b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_print (vfloat V)
{
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vector_complexe_print (vcomplexe V){
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++){
    printf ("%f ", V[i].real) ;
    printf ("%f ", V[i].imaginary) ;
  //}
  printf ("\n") ;
  
  return ;
}

int main (int argc, char **argv)
{
 struct timeval start, end ;
 //unsigned long long int start_tsc, end_tsc ;

 int i ;
 printf ("==========================================================\n") ;
 printf (" |         TEST FONCTIONNALITE - DOUBLE / FLOAT         |\n") ;
 printf ("==========================================================\n") ;
  
  vector_init (vec1, 1.0);     
  vector_init (vec2, 2.0);
  printf("Vec1 = ");
  vector_print(vec1);
  printf("Vec2 = ");
  vector_print(vec2);
  printf("*** Utilisation de la fonction mncblas_sswap...\n");
  mncblas_sswap (VECSIZE, vec1, 1, vec2, 1);
  printf("Vec1 = %f\nVec2 = %f\n", *vec1, *vec2);

 printf ("==========================================================\n") ;
 printf (" |     TEST FONCTIONNALITE - COMPLEXE DOUBLE / FLOAT     |\n") ;
 printf ("==========================================================\n") ;
  
  vector_complexe_init (vec3, 3.0, 5.0);
  vector_complexe_init (vec4, 4.0, 6.0);
  printf("Vec3 = ");
  vector_complexe_print(vec3);
  printf("Vec4 = ");
  vector_complexe_print(vec4);
  printf("*** Utilisation de la fonction mncblas_sswap...\n");
  mncblas_cswap (VECSIZE, vec3, 1, vec4, 1);
  printf("Vec3 = %f %f\nVec4 = %f %f\n", vec3->real, vec3->imaginary, vec4->real, vec4->imaginary);
/*
 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC -  FLOAT               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0) ;
     vector_init (vec2, 2.0) ;
     
     start_tsc = _rdtsc () ;
      mncblas_sswap (VECSIZE, vec1, 1, vec2, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("sswap nano ", 1* VECSIZE, end_tsc-start_tsc) ;
     calcul_debit_tsc ("sswap nano ", 2*4*VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - DOUBLE                |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0) ;
     vector_init_double (vec2d, 2.0) ;
     
     start_tsc = _rdtsc () ;
      mncblas_dswap (VECSIZE, vec1d, 1, vec2d, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("dswap nano ", 1 * VECSIZE, end_tsc-start_tsc) ;
     calcul_debit_tsc ("dswap nano ", 2*8*VECSIZE, end_tsc-start_tsc) ;
   }*/

 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO - FLOAT              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0);
     vector_init (vec2, 2.0);
     
     TOP_MICRO(start) ;
        mncblas_sswap (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_MICRO(end) ;
     
     //calcul_flop_micro ("sswap micro", 1 * VECSIZE, tdiff_micro (&start, &end)) ;
     calcul_debit_micro ("sswap micro", 2*4 * VECSIZE, tdiff_micro (&start, &end)) ;
   } 

  printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO - DOUBLE               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0);
     vector_init_double (vec2d, 2.0);
     
     TOP_MICRO(start) ;
        mncblas_dswap (VECSIZE, vec1d, 1, vec2d, 1) ;
     TOP_MICRO(end) ;
     
     //calcul_flop_micro ("dswap micro", 1 * VECSIZE, tdiff_micro (&start, &end)) ;
     calcul_debit_micro ("dswap micro", 2*8 * VECSIZE, tdiff_micro (&start, &end)) ;
   }
/*
 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE  FLOAT          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 3.0, 5.0);
     vector_complexe_init (vec4, 4.0, 6.0);
     
     start_tsc = _rdtsc () ;
        mncblas_cswap (VECSIZE, vec3, 1, vec4, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("cswap nano: ", 1 * VECSIZE, end_tsc-start_tsc) ;
     calcul_debit_tsc ("cswap nano: ", 2*8 * VECSIZE, end_tsc-start_tsc) ;

   }

 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE DOUBLE         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 3.0, 5.0);
     vector_complexe_init_double (vec4d, 4.0, 6.0);
     
     start_tsc = _rdtsc () ;
        mncblas_zswap (VECSIZE, vec3d, 1, vec4d, 1) ;
     end_tsc = _rdtsc () ;
     
    calcul_flop_tsc ("zswap nano: ", 1 * VECSIZE, end_tsc-start_tsc) ;
     calcul_debit_tsc ("zswap nano: ", 2*16 * VECSIZE, end_tsc-start_tsc) ;
   }
*/
 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE FLOAT         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 3.0, 5.0);
     vector_complexe_init (vec4, 4.0, 6.0);
     
     TOP_MICRO(start) ;
        mncblas_cswap (VECSIZE, vec3, 1, vec4, 1) ;
     TOP_MICRO(end) ;
     
     //calcul_flop_micro ("cswap micro: ", 1 * VECSIZE, tdiff_micro (&start, &end)) ;
     calcul_debit_micro ("cswap micro: ", 2*8 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE DOUBLE       |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 3.0, 5.0);
     vector_complexe_init_double (vec4d, 4.0, 6.0);
     
     TOP_MICRO(start) ;
        mncblas_cswap (VECSIZE, vec3d, 1, vec4d, 1) ;
     TOP_MICRO(end) ;
     
     //calcul_flop_micro ("zswap micro: ", 1 * VECSIZE, tdiff_micro (&start, &end)) ;
     calcul_debit_micro ("zswap micro: ", 2*16 * VECSIZE, tdiff_micro (&start, &end)) ;
   }
}
