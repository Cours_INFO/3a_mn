// test BLAS3
#include <stdio.h>

#include "mnblas.h"
#include "complexe.h"

#include "flop.h"

#define VECSIZE    512
#define MATSIZE    VECSIZE*VECSIZE
#define NB_FOIS    10

typedef float vfloat [VECSIZE] ;
typedef double Mdouble [MATSIZE];
typedef complexe_float_t vcomplexe [VECSIZE] ;
typedef float Mfloat [MATSIZE];
typedef double Mdouble [MATSIZE];
typedef complexe_float_t Mcomplexe [MATSIZE] ;
typedef complexe_double_t Mcomplexe_double [MATSIZE] ;

Mfloat mat1,mat2,mat3;
Mdouble mat1d,mat2d,mat3d;
Mcomplexe mat4,mat5,mat6;
Mcomplexe_double mat4d,mat5d,mat6d;

void matrice_init (Mfloat M, float x)
{
  register unsigned int i ;
  register unsigned int j ;
  for (j = 0; j<VECSIZE; j++){
    for (i = 0; i < VECSIZE; i++)
        M [i+j*VECSIZE] = x ;
  }
  return ;
}

void matrice_init_double (Mdouble M, double x)
{
  register unsigned int i ;
  register unsigned int j ;
  for (j = 0; j<VECSIZE; j++){
    for (i = 0; i < VECSIZE; i++)
        M [i+j*VECSIZE] = x ;
  }
  return ;
}


void matrice_complexe_init (Mcomplexe M, float a, float b)
{
  register unsigned int i ;
  register unsigned int j ;

for (j = 0; j < VECSIZE;j++){
  for (i = 0; i < VECSIZE; i++){
    M[i+j*VECSIZE].real = a ;
    M[i+j*VECSIZE].imaginary = b ;
  }
}
  return ;
}

void matrice_complexe_init_double (Mcomplexe_double M, double a, double b)
{
  register unsigned int i ;
  register unsigned int j ;

for (j = 0; j < VECSIZE;j++){
  for (i = 0; i < VECSIZE; i++){
    M[i+j*VECSIZE].real = a ;
    M[i+j*VECSIZE].imaginary = b ;
  }
}
  return ;
}


void matrice_print (vfloat V)
{
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void matrice_complexe_print (vcomplexe V){
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++){
    printf ("%f ", V[i].real) ;
    printf ("%f ", V[i].imaginary) ;
  //}
  printf ("\n") ;
  
  return ;
}

int main (int argc, char **argv)
{
 struct timeval start, end ;
 unsigned long long int start_tsc, end_tsc ;

 int i ;
// ne marche pas si nombre de coeur est supérieur à la dimension
/* printf ("==========================================================\n") ;
 printf (" |         TEST FONCTIONNALITE - DOUBLE / FLOAT         |\n") ;
 printf ("==========================================================\n") ;
  // test sur des petites structures
  float matrice[4]={1,1,1,1};
  float a=2;
  float matrice2[4]={1,1,1,1};
  float b=1;
  float res[4]={1,1,1,1};
  mncblas_sgemmOpenMP(101,111,111,2,2,0,a,matrice,2,matrice2,2,b,res,2);
  matrice_print(res);
  

 printf ("==========================================================\n") ;
 printf (" |     TEST FONCTIONNALITE - COMPLEXE DOUBLE / FLOAT     |\n") ;
 printf ("==========================================================\n") ;
  // test sur des petites structures
  complexe_float_t matricec[4];
  matricec[0].real=1;
  matricec[1].real=1;
  matricec[2].real=1;
  matricec[3].real=1;
  matricec[0].imaginary=0;
  matricec[1].imaginary=0;
  matricec[2].imaginary=0;
  matricec[3].imaginary=0;
  complexe_float_t c1 = {2.0, 0.0};
  complexe_float_t matrice2c[4];
  matrice2c[0].real=1;
  matrice2c[1].real=1;
  matrice2c[2].real=1;
  matrice2c[3].real=1;
  matrice2c[0].imaginary=0;
  matrice2c[1].imaginary=0;
  matrice2c[2].imaginary=0;
  matrice2c[3].imaginary=0;
  complexe_float_t resc[4];
  resc[0].real=1;
  resc[1].real=1;
  resc[2].real=1;
  resc[3].real=1;
  resc[0].imaginary=0;
  resc[1].imaginary=0;
  resc[2].imaginary=0;
  resc[3].imaginary=0;
  complexe_float_t c2 = {1.0, 0.0};
  mncblas_cgemmOpenMP(101,111,111,2,2,0,&c1,matricec,2,matrice2c,2,&c2,resc,2);
  matrice_complexe_print(resc);
*/
 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - FLOAT               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     
     matrice_init(mat1,1.0);
     matrice_init(mat2,1.0);
     matrice_init(mat3,1.0);

     float alpha=1;
     float beta=1;

     start_tsc = _rdtsc () ;
      mncblas_sgemmOpenMP(101,111,111,VECSIZE,VECSIZE,0,alpha,mat1,VECSIZE,mat2,1,beta,mat3,1);
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("sgemmOpenMP nano ", (long)3*MATSIZE*VECSIZE+2*MATSIZE, end_tsc-start_tsc) ;
   }

 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - DOUBLE                |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     
     matrice_init_double(mat1d,1.0);
     matrice_init_double(mat2d,1.0);
     matrice_init_double(mat3d,1.0);

     float alpha=1;
     float beta=1;

     start_tsc = _rdtsc () ;
      mncblas_dgemmOpenMP(101,111,111,VECSIZE,VECSIZE,0,alpha,mat1d,VECSIZE,mat2d,1,beta,mat3d,1);
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("dgemmOpenMP nano ", (long)3*MATSIZE*VECSIZE+2*MATSIZE, end_tsc-start_tsc) ;
   }

 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO - FLOAT              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     matrice_init(mat1,1.0);
     matrice_init(mat2,1.0);
     matrice_init(mat3,1.0);
     float alpha=1;
     float beta=1;
     
     TOP_MICRO(start) ;
              mncblas_sgemmOpenMP(101,111,111,VECSIZE,VECSIZE,0,alpha,mat1,VECSIZE,mat2,1,beta,mat3,1);
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("sgemmOpenMP micro", (long) 3*MATSIZE*VECSIZE+2*MATSIZE, tdiff_micro (&start, &end)) ;
   }
 
 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO - DOUBLE              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     matrice_init_double(mat1d,1.0);
     matrice_init_double(mat2d,1.0);
     matrice_init_double(mat3d,1.0);
     float alpha=1;
     float beta=1;
     
     TOP_MICRO(start) ;
              mncblas_dgemmOpenMP(101,111,111,VECSIZE,VECSIZE,0,alpha,mat1d,VECSIZE,mat2d,1,beta,mat3d,1);
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("dgemmOpenMP micro", (long) 3*MATSIZE*VECSIZE+2*MATSIZE, tdiff_micro (&start, &end)) ;
   }


 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE FLOAT          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     
     matrice_complexe_init(mat4,1.0,2.0);
     matrice_complexe_init(mat5,1.0,4.0);
     matrice_complexe_init(mat6,1.0,1.0);
     complexe_float_t alpha = {2.0, 0.0};
     complexe_float_t beta = {1.0, 0.0};
     start_tsc = _rdtsc () ;
     mncblas_cgemmOpenMP(101,111,111,VECSIZE,VECSIZE,0,&alpha,mat4,VECSIZE,mat5,1,&beta,mat6,1);
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("cgemmOpenMP nano: ",(long)8*MATSIZE*VECSIZE+6*MATSIZE, end_tsc-start_tsc) ;
   }

 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE DOUBLE          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     
     matrice_complexe_init_double(mat4d,1.0,2.0);
     matrice_complexe_init_double(mat5d,1.0,4.0);
     matrice_complexe_init_double(mat6d,1.0,1.0);
     complexe_float_t alpha = {2.0, 0.0};
     complexe_float_t beta = {1.0, 0.0};
     start_tsc = _rdtsc () ;
     mncblas_zgemmOpenMP(101,111,111,VECSIZE,VECSIZE,0,&alpha,mat4d,VECSIZE,mat5d,1,&beta,mat6d,1);
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("zgemmOpenMP nano: ",(long)8*MATSIZE*VECSIZE+6*MATSIZE, end_tsc-start_tsc) ;
   }


 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE FLOAT         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     matrice_complexe_init(mat4,1.0,2.0);
     matrice_complexe_init(mat5,1.0,4.0);
     matrice_complexe_init(mat6,1.0,1.0);
     complexe_float_t alpha = {2.0, 0.0};
     complexe_float_t beta = {1.0, 0.0};
     TOP_MICRO(start) ;
     mncblas_cgemmOpenMP(101,111,111,VECSIZE,VECSIZE,0,&alpha,mat4,VECSIZE,mat5,1,&beta,mat6,1);     
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("cgemmOpenMP micro: ",(long) 8*MATSIZE*VECSIZE+6*MATSIZE, tdiff_micro (&start, &end)) ;
   }
 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE DOUBLE          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     matrice_complexe_init_double(mat4d,1.0,2.0);
     matrice_complexe_init_double(mat5d,1.0,4.0);
     matrice_complexe_init_double(mat6d,1.0,1.0);
     complexe_float_t alpha = {2.0, 0.0};
     complexe_float_t beta = {1.0, 0.0};
     TOP_MICRO(start) ;
     mncblas_zgemmOpenMP(101,111,111,VECSIZE,VECSIZE,0,&alpha,mat4d,VECSIZE,mat5d,1,&beta,mat6d,1);     
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("zgemmOpenMP micro: ",(long) 8*MATSIZE*VECSIZE+6*MATSIZE, tdiff_micro (&start, &end)) ;
   }

}

/*
  calul de la complexité pour un réel : on fait la multiplication d'une matrice nxn et 
  on sait qu'une addtion sur un complexe nécessite deux addtions et une multiplication 6 opération (4 multiplication et deux additions)
  on a donc pour un complexe: (6+2)n
*/
