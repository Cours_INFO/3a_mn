#include <stdio.h>

#include "mnblas.h"
#include "complexe.h"

#include "flop.h"

#define VECSIZE    65536

#define NB_FOIS    10

typedef float vfloat [VECSIZE] ;
typedef double vdouble [VECSIZE] ;
typedef complexe_float_t vcomplexe [VECSIZE] ;
typedef complexe_double_t vcomplexedouble [VECSIZE] ;


vfloat vec1, vec2 ;
 vdouble vec1d,vec2d;
vcomplexe vec3, vec4 ;
vcomplexedouble vec3d, vec4d;

void vector_init (vfloat V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_init_double (vdouble V, double x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_complexe_init (vcomplexe V, float a, float b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_complexe_init_double (vcomplexedouble V, double a, double b)
{
  register unsigned i;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_print (vfloat V)
{
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vector_complexe_print (vcomplexe V){
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++){
    printf ("%f ", V[i].real) ;
    printf ("%f ", V[i].imaginary) ;
  //}
  printf ("\n") ;
  
  return ;
}

int main (int argc, char **argv)
{
 struct timeval start, end ;
 //unsigned long long int start_tsc, end_tsc ;
 
 int i ;
 vcomplexe alpha;
 printf ("==========================================================\n") ;
 printf (" |         TEST FONCTIONNALITE - DOUBLE / FLOAT         |\n") ;
 printf ("==========================================================\n") ;
  
  vector_init (vec1, 1.0);     
  vector_init (vec2, 2.0);
  printf("Vec1 = ");
  vector_print(vec1);
  printf("Vec2 = ");
  vector_print(vec2);
  printf("*** Utilisation de la fonction mnblas_saxpy...\n");
  mnblas_saxpyOpenMP (VECSIZE, 3.0, vec1, 1, vec2, 1);
  printf("Vec1 = %f\nVec2 = %f\n", *vec1, *vec2);

 printf ("==========================================================\n") ;
 printf (" |     TEST FONCTIONNALITE - COMPLEXE DOUBLE / FLOAT     |\n") ;
 printf ("==========================================================\n") ;
  
  vector_complexe_init (vec3, 3.0, 5.0);
  vector_complexe_init (vec4, 4.0, 6.0);
  vector_complexe_init (alpha, 3.0, 0.0);
  
  printf("Vec3 = ");
  vector_complexe_print(vec3);
  printf("Vec4 = ");
  vector_complexe_print(vec4);
  printf("*** Utilisation de la fonction mnblas_saxpy...\n");
  mnblas_caxpyOpenMP (VECSIZE, alpha, vec3, 1, vec4, 1);
  printf("Vec3 = %f %f\nVec4 = %f %f\n", vec3->real, vec3->imaginary, vec4->real, vec4->imaginary);
/*
 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - FLOAT               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0) ;
     vector_init (vec2, 2.0) ;
     
     start_tsc = _rdtsc () ;
      mnblas_saxpyOpenMP (VECSIZE, 3.0, vec1, 1, vec2, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("saxpy nano ", 2 * VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - DOUBLE                |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0) ;
     vector_init_double (vec2d, 2.0) ;
     
     start_tsc = _rdtsc () ;
      mnblas_daxpyOpenMP (VECSIZE, 3.0, vec1d, 1, vec2d, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("daxpy nano ", 2 * VECSIZE, end_tsc-start_tsc) ;
   }*/
 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO - FLOAT              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0);
     vector_init (vec2, 2.0);
     
     TOP_MICRO(start) ;
        mnblas_saxpyOpenMP (VECSIZE, 3.0, vec1, 1, vec2, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("saxpy micro", 2 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO - DOUBLE               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0);
     vector_init_double (vec2d, 2.0);
     
     TOP_MICRO(start) ;
        mnblas_daxpyOpenMP (VECSIZE, 3.0, vec1d, 1, vec2d, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("daxpy micro", 2 * VECSIZE, tdiff_micro (&start, &end)) ;
   }/*
 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE FLOAT          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 3.0, 5.0);
     vector_complexe_init (vec4, 4.0, 6.0);
     
     start_tsc = _rdtsc () ;
        mnblas_caxpyOpenMP (VECSIZE, alpha, vec3, 1, vec4, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("caxpy nano: ", 8 * VECSIZE, end_tsc-start_tsc) ;
   }
  printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE DOUBLE           |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 3.0, 5.0);
     vector_complexe_init_double (vec4d, 4.0, 6.0);
     
     start_tsc = _rdtsc () ;
        mnblas_zaxpyOpenMP (VECSIZE, alpha, vec3d, 1, vec4d, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("zaxpy nano: ", 8 * VECSIZE, end_tsc-start_tsc) ;
   }
*/
 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE FLOAT         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 3.0, 5.0);
     vector_complexe_init (vec4, 4.0, 6.0);
     
     TOP_MICRO(start) ;
        mnblas_caxpyOpenMP (VECSIZE, alpha, vec3, 1, vec4, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("caxpy micro: ", 8 * VECSIZE, tdiff_micro (&start, &end)) ;
   }
 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE DOUBLE          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 3.0, 5.0);
     vector_complexe_init_double (vec4d, 4.0, 6.0);
     
     TOP_MICRO(start) ;
        mnblas_zaxpyOpenMP (VECSIZE, alpha, vec3d, 1, vec4d, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("zaxpy micro: ", 8 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

}

/*
  calul de la complexité pour un réel : on fait n fois une multiplication et une addition dont 2 n
  on sait qu'une addtion sur un complexe nécessite deux addtions et une multiplication 6 opération (4 multiplication et deux additions)
  on a donc pour un complexe: (6+2)n
*/