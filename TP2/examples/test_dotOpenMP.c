#include <stdio.h>

#include "mnblas.h"
#include "complexe.h"

#include "flop.h"

#define VECSIZE    65536

#define NB_FOIS    10

typedef float vfloat [VECSIZE] ;
typedef double vdouble [VECSIZE] ;
typedef complexe_float_t vcomplexe [VECSIZE] ;
typedef complexe_double_t vcomplexedouble [VECSIZE] ;

vfloat vec1, vec2 ;
vdouble vec1d, vec2d ;
vcomplexe vec3, vec4 ;
vcomplexedouble vec3d, vec4d ;

void vector_init (vfloat V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_init_double (vdouble V, double x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_complexe_init (vcomplexe V, float a, float b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_complexe_init_double (vcomplexedouble V, double a, double b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_print (vfloat V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vector_complexe_print (vcomplexe V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    printf ("%f +i %f", V[i].real,V[i].imaginary) ;
  printf ("\n") ;
  
  return ;
}

int main (int argc, char **argv)
{
 struct timeval start, end ;
 //unsigned long long int start_tsc, end_tsc ;
 
 float res ;
 int i ;
 printf ("==========================================================\n") ;
 printf (" |         TEST FONCTIONNALITE -  FLOAT         |\n") ;
 printf ("==========================================================\n") ;
  
  vector_init (vec1, 1.0);
  vector_init (vec2, 1.0);
  printf("Vec1 = ");
  vector_print(vec1);
  printf("*** Utilisation de la fonction mnblas_sasum...\n");
    res = mncblas_sdotOpenMP (VECSIZE, vec1, 1,vec2,1);
  printf("Vec1 = %f\n", *vec1);

 printf ("res = %f\n", res) ;
 printf ("==========================================================\n") ;
 printf (" |     TEST FONCTIONNALITE - COMPLEXE DOUBLE / FLOAT     |\n") ;
 printf ("==========================================================\n") ;
  
  vector_complexe_init (vec3, 1.0, 2.0);
  printf("Vec3 = ");
  vector_complexe_print(vec3);
  complexe_float_t res_c_float;
  printf("*** Utilisation de la fonction mnblas_sasum...\n");
   mncblas_cdotOpenMPu_sub (VECSIZE, vec3, 1,vec3,1,&res_c_float);
  printf("Vec3 = %f %f\n", vec3->real, vec3->imaginary);
 printf ("res = %f\n", res) ;/*
 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - FLOAT               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0) ;
     vector_init (vec2, 1.0) ;
     
     start_tsc = _rdtsc () ;
        res =  mncblas_sdotOpenMP (VECSIZE, vec1, 1,vec2,1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("sdot nano ", 2*VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - DOUBLE                       |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0) ;
     vector_init_double (vec2d, 1.0) ;

     
     start_tsc = _rdtsc () ;
        res =  mncblas_ddotOpenMP (VECSIZE, vec1d, 1,vec2d,1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("ddot nano ", 2*VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;*/
 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO -  FLOAT              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0);
     vector_init (vec2, 1.0);

     
     TOP_MICRO(start) ;
      res =  mncblas_sdotOpenMP (VECSIZE, vec1, 1,vec2,1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("sdot micro", 2*VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO -  DOUBLE              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0);
     vector_init_double (vec2d, 1.0);

     
     TOP_MICRO(start) ;
      res =  mncblas_ddotOpenMP (VECSIZE, vec1d, 1,vec2d,1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("ddot micro", 2*VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;/*
 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE FLOAT U         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 1.0, 2.0);
    vector_complexe_init (vec4, 1.0, 2.0);
    complexe_float_t res_c_float;

     
     start_tsc = _rdtsc () ;
        mncblas_cdotOpenMPu_sub (VECSIZE, vec3, 1,vec4,1,&res_c_float) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("cdotu_sub nano: ", 8 * VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE DOUBLE  U        |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 1.0, 2.0);
     vector_complexe_init_double (vec4d, 1.0, 2.0);
         complexe_double_t res_c_double;

     start_tsc = _rdtsc () ;
        mncblas_zdotOpenMPu_sub (VECSIZE, vec3d, 1,vec4d,1,&res_c_double) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("zdotu_sub nano: ", 8 * VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;*/
 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE FLOAT  U       |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
    vector_complexe_init (vec3, 1.0, 2.0);
    vector_complexe_init (vec4, 1.0, 2.0);
    complexe_float_t res_c_float;

     
     TOP_MICRO(start) ;
        mncblas_cdotOpenMPu_sub (VECSIZE, vec3, 1,vec4,1,&res_c_float) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("cdotu_sub micro: ", 8 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE DOUBLE  U       |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 1.0, 2.0);
     vector_complexe_init_double (vec4d, 1.0, 2.0);
      complexe_double_t res_c_double;
     
     TOP_MICRO(start) ;
        mncblas_zdotOpenMPu_sub (VECSIZE, vec3d, 1,vec4d,1,&res_c_double) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("zdotu_sub micro: ", 8 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
/*
 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE FLOAT C         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 1.0, 2.0);
    vector_complexe_init (vec4, 1.0, 2.0);
    complexe_float_t res_c_float;

     
     start_tsc = _rdtsc () ;
        mncblas_cdotOpenMPc_sub (VECSIZE, vec3, 1,vec4,1,&res_c_float) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("cdotc_sub nano: ", 8 * VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE DOUBLE  C        |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 1.0, 2.0);
     vector_complexe_init_double (vec4d, 1.0, 2.0);
         complexe_double_t res_c_double;

     start_tsc = _rdtsc () ;
        mncblas_zdotOpenMPc_sub (VECSIZE, vec3d, 1,vec4d,1,&res_c_double) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("zdotC_sub nano: ", 8 * VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;*/
 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE FLOAT  C       |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
    vector_complexe_init (vec3, 1.0, 2.0);
    vector_complexe_init (vec4, 1.0, 2.0);
    complexe_float_t res_c_float;

     
     TOP_MICRO(start) ;
        mncblas_cdotOpenMPc_sub (VECSIZE, vec3, 1,vec4,1,&res_c_float) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("cdotc_sub micro: ", 8 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE DOUBLE  C       |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 1.0, 2.0);
     vector_complexe_init_double (vec4d, 1.0, 2.0);
         complexe_double_t res_c_double;
     
     TOP_MICRO(start) ;
        mncblas_zdotOpenMPc_sub (VECSIZE, vec3d, 1,vec4d,1,&res_c_double) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("zdotc_sub micro: ", 8 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;

}

/*
  calul de la complexité pour un réel : on fait n fois une multiplication et une addition dont 2 n
  On sait qu'une addtion sur un complexe nécessite deux addtions et une multiplication 6 opérations (4 multiplication et deux additions)
  On a donc pour un complexe: (6+2)n
*/