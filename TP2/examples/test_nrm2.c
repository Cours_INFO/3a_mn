#include <stdio.h>

#include "mnblas.h"
#include "complexe.h"

#include "flop.h"

#define VECSIZE    65536

#define NB_FOIS    10

typedef float vfloat [VECSIZE] ;
typedef double vdouble [VECSIZE] ;
typedef complexe_float_t vcomplexe [VECSIZE] ;
typedef complexe_double_t vcomplexedouble [VECSIZE] ;

vfloat vec1;
vdouble vec1d ;
vcomplexe vec3 ;
vcomplexedouble vec3d;

void vector_init (vfloat V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_init_double (vdouble V, double x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_complexe_init (vcomplexe V, float a, float b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_complexe_init_double (vcomplexedouble V, double a, double b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_print (vfloat V)
{
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vector_complexe_print (vcomplexe V){
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++){
    printf ("%f ", V[i].real) ;
    printf ("%f ", V[i].imaginary) ;
  //}
  printf ("\n") ;
  
  return ;
}

int main (int argc, char **argv)
{
 struct timeval start, end ;
 //unsigned long long int start_tsc, end_tsc ;
 
 float res ;
 int i ;
 printf ("==========================================================\n") ;
 printf (" |         TEST FONCTIONNALITE - DOUBLE / FLOAT         |\n") ;
 printf ("==========================================================\n") ;
  
  vector_init (vec1, 1.0);
  printf("Vec1 = ");
  vector_print(vec1);
  printf("*** Utilisation de la fonction mnblas_snrm2...\n");
    res = mnblas_snrm2 (VECSIZE, vec1, 1);
  //printf("Vec1 = %f\n", *vec1);

 printf ("res = %f\n", res) ;
 printf ("==========================================================\n") ;
 printf (" |     TEST FONCTIONNALITE - COMPLEXE DOUBLE / FLOAT     |\n") ;
 printf ("==========================================================\n") ;
  
  vector_complexe_init (vec3, 1.0, 2.0);
  printf("Vec3 = ");
  vector_complexe_print(vec3);
  printf("*** Utilisation de la fonction mnblas_scnrm2...\n");
  res = mnblas_scnrm2 (VECSIZE, vec3, 1);
  //printf("Vec3 = %f %f\n", vec3->real, vec3->imaginary);

 printf ("res = %f\n", res) ;/*
 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - FLOAT               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0) ;
     
     start_tsc = _rdtsc () ;
        res =  mnblas_snrm2 (VECSIZE, vec1, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("snrm2 nano ", 2 * VECSIZE+1, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;

printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - DOUBLE               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0) ;
     
     start_tsc = _rdtsc () ;
        res =  mnblas_dnrm2 (VECSIZE, vec1d, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("dnrm2 nano ", 2 * VECSIZE+1, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;*/
 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO - FLOAT              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0);
     
     TOP_MICRO(start) ;
        res = mnblas_snrm2 (VECSIZE, vec1, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("snrm2 micro", 2 * VECSIZE+1, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;

  printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO - DOUBLE              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0);
     
     TOP_MICRO(start) ;
        res = mnblas_dnrm2 (VECSIZE, vec1d, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("dnrm2 micro", 2 * VECSIZE+1, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;/*
 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE FLOAT          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 1.0, 2.0);
     
     start_tsc = _rdtsc () ;
        res = mnblas_scnrm2 (VECSIZE, vec3, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("scnrm2 nano: ", 4 * VECSIZE+1, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;

  printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE DOUBLE          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 1.0, 2.0);
     
     start_tsc = _rdtsc () ;
        res = mnblas_dznrm2 (VECSIZE, vec3d, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("dznrm2 nano: ", 4 * VECSIZE+1, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;*/
 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE  FLOAT         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 1.0, 2.0);
     
     TOP_MICRO(start) ;
        res = mnblas_scnrm2 (VECSIZE, vec3, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("scnrm2 micro: ", 4 * VECSIZE+1, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;

  printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE DOUBLE       |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 1.0, 2.0);
     
     TOP_MICRO(start) ;
        res = mnblas_dznrm2 (VECSIZE, vec3d, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("dznrm2 micro: ", 4 * VECSIZE+1, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
}
//
// pour le calcul de la norme simple:
// somme de produit de deux v[i] et le tout mis à la racine carré
// donc 2N+1
//pour les complexe
// somme (somme des produits des réel et des imaginaire de v[i] et on met à la racine) puis mis à la racine
// N(2+1+1)+1
//