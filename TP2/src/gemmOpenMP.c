#include "mnblas.h"
#include "complexe2.h"
#include <stdio.h>

#define numberthread 2

void mncblas_sgemmOpenMP(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc){
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    register unsigned int k = 0 ;
    int n_per_thread=N/numberthread;
    omp_set_num_threads(numberthread);
    #pragma omp parallel for shared(A, B,C,M) private(i,j,k) schedule(static, n_per_thread)
    for ( i = 0 ; i < N ; i++ ){ 
        for ( j = 0 ; j < M ; j++ ){
            float summ = 0;
            for ( k = 0 ; k < M ; k++ ){
                summ +=alpha*A[k+i*M]*B[j+k*M];
            }
            C[i+j*M]= summ+beta*C[i+j*M];
        }   
    }
}

void mncblas_dgemmOpenMP(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc){
    double summ;
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    register unsigned int k = 0 ;
    int n_per_thread=N/numberthread;
    omp_set_num_threads(numberthread);
    #pragma omp parallel for shared(A, B,C,M) private(summ,i,j,k) schedule(static, n_per_thread)
    for ( i = 0 ; i < N ; i++ ){ 
        for ( j = 0 ; j < M ; j++ ){
            summ = 0;
            for ( k = 0 ; k < M ; k++ ){
                summ +=alpha*A[k+i*M]*B[j+k*M];
            }
            C[i+j*M]= summ+beta*C[i+j*M];
        }   
    }
}


void mncblas_cgemmOpenMP(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc){
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    register unsigned int k = 0 ;
    complexe_float_t summ;

    const complexe_float_t* A1=A;
    const complexe_float_t* B1=B;
    complexe_float_t* C1=C;
    const complexe_float_t* ALPHA=alpha;
    const complexe_float_t* BETA=beta;

    int n_per_thread=N/numberthread;
    omp_set_num_threads(numberthread);
    #pragma omp parallel for shared(A1, B1,C1,M) private(summ,i,j,k) schedule(static, n_per_thread) 
    for ( i = 0 ; i < N ; i++ ){
        for ( j = 0 ; j < M ; j++ ){
            summ.real = 0;
            summ.imaginary=0;
            for ( k = 0 ; k < M ; k++ ){
                summ =add_complexe_float(mult_complexe_float(mult_complexe_float(*ALPHA,A1[k+i*M]),B1[j+k*M]),summ);
            }
            C1[i+j*M] = add_complexe_float(summ,mult_complexe_float(*BETA,C1[i+j*M]));
        }
    }
}

void mncblas_zgemmOpenMP(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc){
                    
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    register unsigned int k = 0 ;
    complexe_double_t summ;
                    
    const complexe_double_t* A1=A;
    const complexe_double_t* B1=B;
    complexe_double_t* C1=C;
    const complexe_double_t* ALPHA=alpha;
    const complexe_double_t* BETA=beta;
                    
    int n_per_thread=N/numberthread;
    omp_set_num_threads(numberthread);
    #pragma omp parallel for shared(A1, B1,C1,M) private(summ,i,j,k) schedule(static, n_per_thread) 
    for ( i = 0 ; i < N ; i++ ){
        
        for ( j = 0 ; j < M ; j++ ){
            summ.real = 0;
            summ.imaginary=0;
            for ( k = 0 ; k < M ; k++ ){
                summ =add_complexe_double(mult_complexe_double(mult_complexe_double(*ALPHA,A1[k+i*M]),B1[j+k*M]),summ);
            }
                C1[i+j*M]=add_complexe_double(summ,mult_complexe_double(*BETA,C1[i+j*M]));
        }
        
    }
}