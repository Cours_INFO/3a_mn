#include "mnblas.h"
#include "complexe2.h"

/***   Computes the sum of magnitudes of the vector elements.   ***/

/*  float  */
float  mnblas_sasum(const int N, const float *X, const int incX){
    float res = 0.0;
      register unsigned int i = 0 ;

    for ( i = 0; i < N; i += incX) {
        if(X[i] >= 0){
            res += X[i];
        } else {
            res -= X[i]; 
        }
    }
    return res;    
}

/*  double  */
double mnblas_dasum(const int N, const double *X, const int incX){
    double res = 0.0;
      register unsigned int i = 0 ;
    for ( i = 0; i < N; i += incX) {
        if(X[i] >= 0){
            res += X[i];
        } else {
            res -= X[i]; 
        }
    }
    return res;    
}

/*  float complex  */
float  mnblas_scasum(const int N, const void *X, const int incX){
    float res = 0.0;
    const complexe_float_t *comp_X = X;
    register unsigned int i = 0 ;
    for ( i = 0; i < N; i += incX) {
        if(comp_X[i].real >= 0){
            res += comp_X[i].real;
        } else {
            res -=comp_X[i].real; 
        }
        if(comp_X[i].imaginary >= 0){
            res +=comp_X[i].imaginary;
        } else {
            res -=comp_X[i].imaginary; 
        }
    }
    return res;    
}

/*  double complex  */
double mnblas_dzasum(const int N, const void *X, const int incX){
    double res = 0.0;
    const complexe_double_t *comp_X = X;
    register unsigned int i = 0 ;
    for ( i = 0; i < N; i += incX) {
       if(comp_X[i].real >= 0){
            res += comp_X[i].real;
        } else {
            res -=comp_X[i].real; 
        }
        if(comp_X[i].imaginary >= 0){
            res +=comp_X[i].imaginary;
        } else {
            res -=comp_X[i].imaginary; 
        }
    }
    return res;    
}