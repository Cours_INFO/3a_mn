#include "mnblas.h"
#include "complexe2.h"
#include <stdio.h>

#define numberthread 8
/*
float mncblas_sdot(const int N, const float *X, const int incX, 
                 const float *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register float dot = 0.0 ;
  
  for (; ((i < N) && (j < N)) ; i += incX, j+=incY) {
      dot = dot + X [i] * Y [j] ;
    }

  return dot ;
}
*/

/**
 * > The function `mncblas_sdotOpenMP` computes the dot product of two vectors `X` and `Y` of size `N`
 * using OpenMP
 * 
 * @param N the number of elements in the vectors
 * @param X The first vector
 * @param incX The increment for the elements of X.
 * @param Y The vector to be multiplied
 * @param incY The increment for the elements of Y.
 * 
 * @return The dot product of the two vectors.
 */
float mncblas_sdotOpenMP(const int N, const float *X, const int incX, 
                 const float *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  float dot = 0.0 ;
  omp_set_num_threads(numberthread);
  #pragma omp parallel for reduction(+:j) reduction(+:dot)
  for (i = 0 ; i < N ; i += incX) {
      dot += X[i]*Y[j];
      j+=incY ;
    }

  return dot ;
}

/**
 * > The function `mncblas_ddotOpenMP` computes the dot product of two vectors `X` and `Y` of size `N`
 * using OpenMP
 * 
 * @param N the number of elements in the vectors
 * @param X the first vector
 * @param incX The increment for the elements of X.
 * @param Y The vector to be multiplied
 * @param incY The increment for the elements of Y.
 * 
 * @return The dot product of the two vectors.
 */
double mncblas_ddotOpenMP(const int N, const double *X, const int incX, 
                 const double *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  double dot = 0.0 ;
  omp_set_num_threads(numberthread);
  #pragma omp parallel for reduction(+:j) reduction(+:dot)
  for (i = 0 ; i < N ; i += incX) {
      dot += X[i]*Y[j];
      j+=incY ;
    }

  return dot ;
}

/**
 * > The function computes the dot product of two complex vectors, X and Y
 * (and stores the result in dotu)
 * 
 * @param N the number of elements in the vectors X and Y
 * @param X The first vector
 * @param incX The increment for the elements of X.
 * @param Y The vector to be multiplied
 * @param incY The increment for the elements of Y.
 * @param dotu the result of the dot product
 */
void   mncblas_cdotOpenMPu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  complexe_float_t* dot = (complexe_float_t*) dotu;
  dot->real=0.0;
  dot->imaginary=0.0;
  
  const complexe_float_t* C1=X;
  const complexe_float_t* C2=Y;
  omp_set_num_threads(numberthread);
  #pragma omp parallel for reduction(+:j)    //private(dot)
  for (i = 0 ; i < N ; i += incX) {
      *dot = add_complexe_float(*dot,mult_complexe_float (C1[i],C2[j])) ;
      j+=incY ;
    }
}

/**
 * It computes the dot product of two vectors
 * 
 * @param N the number of elements in the vectors
 * @param X The first vector
 * @param incX The increment for the elements of X.
 * @param Y The vector to be multiplied
 * @param incY The increment for the elements of Y.
 * @param dotc the result of the dot product
 */
void   mncblas_cdotOpenMPc_sub(const int N,const void *X, const int incX,
                       const void *Y, const int incY, void *dotc)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  complexe_float_t* dot = (complexe_float_t*) dotc;
  dot->real=0.0;
  dot->imaginary=0.0;
  
  const complexe_float_t* C1=X;
  const complexe_float_t* C2=Y;
  omp_set_num_threads(numberthread);
  #pragma omp parallel for reduction(+:j)   //private(dot)
  for (i = 0 ; i < N ; i += incX) {
      *dot = add_complexe_float(*dot,mult_complexe_float (conj_complexe_float(C1[i]),C2[j])) ;
      j+=incY ;
    }
}


/**
 * It computes the dot product of two vectors
 * 
 * @param N the number of elements in the vectors X and Y
 * @param X The first vector
 * @param incX The increment for the elements of X.
 * @param Y The vector Y
 * @param incY The increment for the elements of Y.
 * @param dotu the result of the dot product
 */
void   mncblas_zdotOpenMPu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  complexe_double_t* dot = (complexe_double_t*) dotu;
  dot->real=0.0;
  dot->imaginary=0.0;
  
  const complexe_double_t* C1=X;
  const complexe_double_t* C2=Y;
  omp_set_num_threads(numberthread);
  #pragma omp parallel for reduction(+:j) //private(dot)
  for (i = 0 ; i < N ; i += incX) {
      *dot = add_complexe_double(*dot,mult_complexe_double (C1[i],C2[j])) ;
      j+=incY ;
    }
}

/**
 * It computes the dot product of two vectors
 * 
 * @param N the number of elements in the vectors X and Y
 * @param X The first vector
 * @param incX The increment for the elements of X.
 * @param Y The vector to be multiplied
 * @param incY The increment for the elements of Y.
 * @param dotc the result of the dot product
 */
void   mncblas_zdotOpenMPc_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  complexe_double_t* dot = (complexe_double_t*) dotc;
  dot->real=0.0;
  dot->imaginary=0.0;
  
  const complexe_double_t* C1=X;
  const complexe_double_t* C2=Y;
  omp_set_num_threads(numberthread);
  #pragma omp parallel for reduction(+:j) // private(dot)
  for (i = 0 ; i < N ; i += incX) {
      *dot = add_complexe_double(*dot,mult_complexe_double (conj_complexe_double (C1[i]),C2[j])) ;
      j+=incY ;
    }
}





