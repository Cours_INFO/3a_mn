#include "mnblas.h"
#include "complexe.h"

/*  entier  */
void mncblas_sswap(const int N, float *X, const int incX, 
                 float *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register float save ;
  
  for (; ((i < N) && (j < N)) ; i += incX, j+=incY) {
      save = Y [j] ;
      Y [j] = X [i] ;
      X [i] = save ;
    }

  return ;
}

/*  double  */
void mncblas_dswap(const int N, double *X, const int incX, 
                 double *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register double save ;
  
  for (; ((i < N) && (j < N)) ; i += incX, j+=incY) {
      save = Y [j] ;
      Y [j] = X [i] ;
      X [i] = save ;
    }

  return ;
}

/*  complex  */
void mncblas_cswap(const int N, void *X, const int incX, 
		                    void *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_float_t save ;
  
  complexe_float_t *comp_X = X;
  complexe_float_t *comp_Y = Y;

  for (; ((i < N) && (j < N)) ; i += incX, j+=incY) {
      save.real = comp_Y[j].real ;
      save.imaginary = comp_Y[j].imaginary ;
      comp_Y[j].real = comp_X[i].real ;
      comp_Y[j].imaginary = comp_X[i].imaginary ;
      comp_X[i].real = save.real ;
      comp_X[i].imaginary = save.imaginary ;
    }

  return ;
}

/*  double complex  */
void mncblas_zswap(const int N, void *X, const int incX, 
		                    void *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_double_t save ;
  
  complexe_double_t *comp_X = X;
  complexe_double_t *comp_Y = Y;

  for (; ((i < N) && (j < N)) ; i += incX, j+=incY) {
      save.real = comp_Y[j].real ;
      save.imaginary = comp_Y[j].imaginary ;
      comp_Y[j].real = comp_X[i].real ;
      comp_Y[j].imaginary = comp_X[i].imaginary ;
      comp_X[i].real = save.real ;
      comp_X[i].imaginary = save.imaginary ;
    }

  return ;
}

