#include "mnblas.h"
#include "complexe2.h"

/***   Computes a vector-scalar product and adds the result to a vector.   ***/
/*      y := a*x + y    */

/*  float  */
void mnblas_saxpy(const int N, const float alpha, const float *X,
                 const int incX, float *Y, const int incY){
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;

    for( i = 0; i < N; i += incX){
        Y[j] = alpha * X[i] + Y[j];
        j += incY;
    }
}

/*  double  */
void mnblas_daxpy(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY){
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    for( i = 0; i < N; i += incX){
        Y[j] = alpha * X[i] + Y[j];
        j += incY;
    }
}

/*  float complex  */
void mnblas_caxpy(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY){
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;

    const complexe_float_t *comp_X = X;
    complexe_float_t *comp_Y = Y;
    const complexe_float_t *a=alpha;
    complexe_float_t tmp_mult;
    for( i = 0; i < N; i += incX){
        tmp_mult = mult_complexe_float(*a,comp_X[i]);
        comp_Y[j] = add_complexe_float(tmp_mult ,comp_Y[j]);
        j += incY;
    }
}

/*  double complex  */
void mnblas_zaxpy(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY){
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    const complexe_double_t *comp_X = X;
    complexe_double_t *comp_Y = Y;
    const complexe_double_t *a=alpha;
    complexe_double_t tmp_mult;
    for( i = 0; i < N; i += incX){
        tmp_mult = mult_complexe_double(*a,comp_X[i]);
        comp_Y[j] = add_complexe_double(tmp_mult ,comp_Y[j]);
        j += incY;
    }
}