#include "complexe.h"

complexe_float_t add_complexe_float (const complexe_float_t c1, const complexe_float_t c2)
{
  complexe_float_t r ;

  r.real = c1.real + c2.real ;
  r.imaginary = c1.imaginary + c2.imaginary ;
  
  return r ;
}

complexe_double_t add_complexe_double (const complexe_double_t c1, const complexe_double_t c2)
{
  complexe_double_t r ;

  r.real = c1.real + c2.real ;
  r.imaginary = c1.imaginary + c2.imaginary ;
  
  return r ;
}

complexe_float_t mult_complexe_float (const complexe_float_t c1, const complexe_float_t c2)
{
  complexe_float_t r ;

  /* C1/C2 = R1*R2+R1*I2+I1*R2+I1*I2
  */

  r.real = c1.real*c2.real + (c1.imaginary*c2.imaginary)*-1 ;
  r.imaginary = c1.real*c2.imaginary + c2.real*c1.imaginary ;
  
  return r ;
}

complexe_double_t mult_complexe_double (const complexe_double_t c1, const complexe_double_t c2)
{
  complexe_double_t r ;

  /* C1/C2 = R1*R2+R1*I2+I1*R2+I1*I2
  */
  
  r.real = c1.real*c2.real - (c1.imaginary*c2.imaginary) ;
  r.imaginary = c1.real*c2.imaginary + c2.real*c1.imaginary ;
  
  return r ;
}
  

complexe_float_t div_complexe_float (const complexe_float_t c1, const complexe_float_t c2)
{
  complexe_float_t r ;

  /* C1/C2  = (R1+I1)/(R2+I2)
            = (R1+I1)*(R2-I2)/(R2+I2)*(R2-I2)
            = (R1*R2+R1*I2+I1*R2+I1*I2)/(R2*R2+R2*(-I2)+I2*R2+(-I2)*I2)
            = (R1*R2+R1*I2+I1*R2+I1*I2)/(R2*R2-(I2*I2))
  */
  complexe_float_t c2_conjuguer , c_numerateur, c_denominateur;
  c2_conjuguer = conj_complexe_float(c2);

  c_numerateur = mult_complexe_float (c1, c2_conjuguer);
  c_denominateur = mult_complexe_float (c2, c2_conjuguer);

  r.real = (c_numerateur.real)/ c_denominateur.real;  //(c2.real*c2.real - c2.imaginary*c2.imaginary)
  r.imaginary = (c_numerateur.imaginary)/ c_denominateur.real;
  
  return r ;
}

complexe_double_t div_complexe_double (const complexe_double_t c1, const complexe_double_t c2)
{
  complexe_double_t r ;

  /* C1/C2  = (R1+I1)/(R2+I2)
            = (R1+I1)*(R2-I2)/(R2+I2)*(R2-I2)
            = (R1*R2+R1*I2+I1*R2+I1*I2)/(R2*R2+R2*(-I2)+I2*R2+(-I2)*I2)
            = (R1*R2+R1*I2+I1*R2+I1*I2)/(R2*R2-(I2*I2))
  */
  complexe_double_t c2_conjuguer , c_numerateur, c_denominateur;
  c2_conjuguer = conj_complexe_double(c2);

  c_numerateur = mult_complexe_double (c1, c2_conjuguer);
  c_denominateur = mult_complexe_double (c2, c2_conjuguer);

  r.real = (c_numerateur.real)/ c_denominateur.real;  //(c2.real*c2.real - c2.imaginary*c2.imaginary)
  r.imaginary = (c_numerateur.imaginary)/ c_denominateur.real;
  
  return r ;
}

complexe_float_t conj_complexe_float (const complexe_float_t c1){

  complexe_float_t r ;

  r.real = c1.real;
  r.imaginary = (-1)*c1.imaginary;
  
  return r;
}

complexe_double_t conj_complexe_double (const complexe_double_t c1){

  complexe_double_t r ;

  r.real = c1.real;
  r.imaginary = (-1)*c1.imaginary;
  
  return r;
}