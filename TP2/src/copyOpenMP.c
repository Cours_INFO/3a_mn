#include "mnblas.h"
#include "complexe2.h"

#define numberthread 1


/***   Copies vector to another vector.   ***/

/**
 * > It copies the vector X into the vector Y for float
 * 
 * @param N the number of elements in the vector
 * @param X the source vector
 * @param incX The increment for the elements of X.
 * @param Y The destination array.
 * @param incY The increment for the elements of Y.
 * 
 * @return Nothing.
 */
void mncblas_scopyOpenMP(const int N, const float *X, const int incX, 
                 float *Y, const int incY)
{
  register unsigned int i = 0 ;
  int n_per_thread=N/numberthread;

  //int limit = min(N/incX,N/incX);
  int limit = (N/incX < N/incY)? N/incX: N/incY;
  omp_set_num_threads(numberthread);
  #pragma omp parallel for shared(X, Y) private(i) schedule(static, n_per_thread)
  for (i = 0; i < limit; i++) { //for (; ((i < N) && (j < N)) ; i += incX, j += incY)
    Y [i*incY] = X [i*incX] ;
  }

  return ;
}

/**
 * > It copies the vector X into the vector Y for double
 * 
 * @param N the number of elements in the vector
 * @param X the source vector
 * @param incX The increment for the elements of X.
 * @param Y The destination array.
 * @param incY The increment for the elements of Y.
 * 
 * @return Nothing.
 */
void mncblas_dcopyOpenMP(const int N, const double *X, const int incX, 
                 double *Y, const int incY)
{
  register unsigned int i = 0 ;
  int n_per_thread=N/numberthread;

  //int limit = min(N/incX,N/incX);
  int limit = (N/incX < N/incY)? N/incX: N/incY;
  omp_set_num_threads(numberthread);
  #pragma omp parallel for shared(X, Y) private(i) schedule(static, n_per_thread)
  for (i = 0; i < limit; i ++) { //for (; ((i < N) && (j < N)) ; i += incX, j += incY)
    Y [i*incY] = X [i*incX] ;
  }

  return ;

}

/**
 * It copies the vector X into the vector Y for float complex
 * 
 * @param N the number of elements in the vector
 * @param X The source vector
 * @param incX The increment for the elements of X.
 * @param Y The destination array.
 * @param incY The increment for the elements of Y.
 * 
 * @return Nothing.
 */
void mncblas_ccopyOpenMP(const int N, const void *X, const int incX, 
		                    void *Y, const int incY)
{
  register unsigned int i = 0 ;
  int n_per_thread=N/numberthread;

  const complexe_float_t *comp_X = X;
  complexe_float_t *comp_Y = Y;

  //int limit = min(N/incX,N/incX);
  int limit = (N/incX < N/incY)? N/incX: N/incY;
  omp_set_num_threads(numberthread);
  #pragma omp parallel for shared(comp_X, comp_Y) private(i) schedule(static, n_per_thread)
  for (i = 0; i < limit; i ++) { //for (; ((i < N) && (j < N)) ; i += incX, j += incY)
    comp_Y[i*incY] = comp_X[i*incX] ;
  }
    
  return ;
}

/**
 * It copies the vector X into the vector Y for double complex
 * 
 * @param N the number of elements in the vector
 * @param X The source vector
 * @param incX The increment for the elements of X.
 * @param Y The destination array.
 * @param incY The increment for the elements of Y.
 * 
 * @return Nothing.
 */
void mncblas_zcopyOpenMP(const int N, const void *X, const int incX, 
		                    void *Y, const int incY)
{
  register unsigned int i = 0 ;
  int n_per_thread=N/numberthread;

  const complexe_double_t *comp_X = X;
  complexe_double_t *comp_Y = Y;

  //int limit = min(N/incX,N/incX);
  int limit = (N/incX < N/incY)? N/incX: N/incY;
  omp_set_num_threads(numberthread);
  #pragma omp parallel for shared(comp_X, comp_Y) private(i) schedule(static, n_per_thread)
  for (i = 0; i < limit; i ++) {//for (; ((i < N) && (j < N)) ; i += incX, j += incY)
    comp_Y[i*incY] = comp_X[i*incX] ;
  }
    
  return ;
}

