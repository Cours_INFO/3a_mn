# Test Performance

## Sujet

    Tests en c, en python et en java pour comparer les différents temps d'exécuter entre ces différents languages et les différentes combinaisons pour les triples boucles for imbriquées, par exemple, avec les variables i, j, k dans différents ordres comme ijk, ikj, kij, ...

## Programmes:

 | en C | en JAVA | en Python |
 |:-----|:--------|:----------|
 | [matrice.c](./Test_Performance/matrice.c) | [mm_java.java](./Test_Performance/mm_java.java) | [mm.py](./Test_Performance/mm.py) |


***

# TP1 - Polynômes Pleins et Creux

* **[Sujet](./docs/TP1MN.pdf)**

* [Archive des squelettes des codes](./archive)

***

# Test Performance & TP1 - Polynômes Pleins et Creux

* **[Compte-Rendu](./docs/CR_MN_Test_Perf-TP1.pdf)**
