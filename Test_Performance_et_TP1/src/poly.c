#include <stdio.h>
#include <stdlib.h>

#include "../lib/poly.h"

#include <x86intrin.h>

p_polyf_t creer_polynome (int degre) {
  p_polyf_t p ;
  
  p = (p_polyf_t) malloc (sizeof (polyf_t)) ;
  p->degre = degre ;

  p->coeff = (float *) malloc ((degre+1) * sizeof (float))  ;

  return p ;
}

void detruire_polynome (p_polyf_t p) {
  free (p->coeff) ;
  free (p) ;
  return ;
}

void init_polynome (p_polyf_t p, float x) {
  register unsigned int i ;

  for (i = 0 ; i <= p->degre; ++i)
    p->coeff [i] = x ;

  return ;
}

p_polyf_t lire_polynome_float (char *nom_fichier) {
  FILE *f ;
  p_polyf_t p ;
  int degre ;
  int i  ;
  int cr ;
  
  f = fopen (nom_fichier, "r") ;
  if (f == NULL) {
    fprintf (stderr, "erreur ouverture %s \n", nom_fichier) ;
    exit (-1) ;
  }
  
  cr = fscanf (f, "%d", &degre) ;
  if (cr != 1) {
    fprintf (stderr, "erreur lecture du degre\n") ;
    exit (-1) ;
  }
  p = creer_polynome (degre) ;
  
  for (i = 0 ; i <= degre; i++) { 
    cr = fscanf (f, "%f", &p->coeff[i]) ;
      if (cr != 1) {
      fprintf (stderr, "erreur lecture coefficient %d\n", i) ;
      exit (-1) ;
    }   
  }

  fclose (f) ;

  return p ;
}

void ecrire_polynome_float (p_polyf_t p) {
  int i ;

  printf ("%f + %f x ", p->coeff [0], p->coeff [1]) ;
  
  for (i = 2 ; i <= p->degre; i++) {  
    printf ("+ %f X^%d ", p->coeff [i], i) ;
  }
  
  printf ("\n") ;
  return ;
}

int egalite_polynome (p_polyf_t p1, p_polyf_t p2) {
  /*  Vérifier que les degres sont identiques   */
  if(p1->degre != p2->degre)
    return 0;

  /*  Vérifier que tous les coefficients sont identiques   */
  for(int i = 0; i <= p1->degre; i++) {
    if(p1->coeff[i] != p2->coeff[i])
      return 0;
  }

  /*  Si les vérifications sont arrivées jusqu'ici, 
   *  alors les deux polynomes sont égaux.   
   */
  return 1;
}

p_polyf_t addition_polynome (p_polyf_t p1, p_polyf_t p2) {
  p_polyf_t p3 ;
  register unsigned int i ;
  
  p3 = creer_polynome (max (p1->degre, p2->degre));

  for (i = 0 ; i <= min (p1->degre, p2->degre); ++i) {
    p3->coeff [i] = p1->coeff [i] + p2->coeff [i] ;
  }

  if (p1->degre > p2->degre) {
    for (i = (p2->degre + 1) ; i <= p1->degre; ++i)
	p3->coeff [i] = p1->coeff [i] ;
  } else if (p2->degre > p1->degre) {
    for (i = (p1->degre + 1) ; i <= p2->degre; ++i)
	p3->coeff [i] = p2->coeff [i] ;
  }
    
  return p3 ;
}

p_polyf_t multiplication_polynome_scalaire (p_polyf_t p, float alpha) {
  /*  p_res = alpha * p
   *  Pour cela, on créer un nouveau polynome, qui sera le resultat
   *  de la multiplication p * alpha, i.e tous les coefficients de
   *  p fois alpha.
   *  alpha n'influance pas le degré du polynome
   */
  
  p_polyf_t p_res = creer_polynome(p->degre);

  for(int i = 0; i <= p->degre; i++)
    p_res->coeff[i] = p->coeff[i] * alpha;
  
  return p_res ;
}

float puissance(float val, int exp) {
  float f_res = 1.0;
  if(exp >= 1) {
    for (int i = 1; i <= exp; i++)
      f_res *= val;
  }
 // printf("    %d:%f - ",exp, f_res);
  return f_res;
}

float eval_polynome (p_polyf_t p, float x) {
  /* 
     valeur du polynome pour la valeur de x
  */

  float fres = 0;
  //int puissance = x;
  for(int i = 0; i <= p->degre; i++)
    fres += p->coeff[i] * puissance(x, i);
  return fres ;
}


p_polyf_t multiplication_polynomes (p_polyf_t p1, p_polyf_t p2) {
  /*  p_res = p1 * p2
   *  Pour cela, on créer un nouveau polynome, qui sera le resultat
   *  de la multiplication p1 * p2. De plus, nous utilisons deux
   *  boucles imbriquées, pour effectuer la distributivité. D'après
   *  une propriété des puissances, le polynome p_res aura un degré
   *  égal à la somme des degrés des éléments du produit.
   */

  /*  = (X^2 + X + 3) * (X^3 + 2X^1 + 3X^0)
      = X^2 * (X^3 + 2X^1 + 3X^0) + X * (X^3 + 2X^1 + 3X^0)
        + 3 * (X^3 + 2X^1 + 3X^0)
      = X^2 * X^3 + X^2 * 2X^1 + X^2 * 3X^0 + ...
      = X^(2+3) + X^(2+1) + X^(2+0) + ...
      = X^5 + X^3 + X^2 + ...
  */

  p_polyf_t result= creer_polynome(p1->degre+p2->degre);
  for (int i=0;i<=result->degre;i++) {
    result->coeff[i]=0;
  }
  for (int i=0;i<=p1->degre;i++) {
    for (int j=0;j<=p2->degre;j++) {
      result->coeff[i+j]=result->coeff[i+j]+p1->coeff[i]*p2->coeff[j];
    }
  }

  return result ;
}

p_polyf_t puissance_polynome (p_polyf_t p, int n) {
  /*  p_res = p^n
   *  Pour cela, on créer un nouveau polynome, qui sera le resultat
   *  de p à la puissance p1^n.
   *  D'après une propriété des puissances, le polynome p_res aura 
   *  un degré égal au produit des degrés/puissances.
   */
  /*  = (X^5)^3 
      = X^(5*3)
      = X^15
  */
  /* 
     p^n
  */
  p_polyf_t result= creer_polynome(0);

  init_polynome(result,1);
  for (int i=1;i<=n;i++) {
    result=multiplication_polynomes(result,p);
  }

  return result;
}

p_polyf_t composition_polynome (p_polyf_t p, p_polyf_t q) {
  /*  p O q
  */
  p_polyf_t result = creer_polynome(q->degre);
  p_polyf_t temp = creer_polynome(q->degre);
  p_polyf_t puissance = creer_polynome(q->degre);
  int i;

  init_polynome(result, 0.0);

  result->coeff[0] = p->coeff[0];
  for(i = 0; i <= q->degre; i++)
    puissance->coeff[i] = q->coeff[i];
  
  for(i = 1;i <= p->degre;i++) {
    temp = multiplication_polynome_scalaire(puissance,p->coeff[i]);
    puissance = multiplication_polynomes(puissance,q);
    result = addition_polynome(result,temp);
  }

  detruire_polynome(temp);
  detruire_polynome(puissance);
  return result ;
}

/******************************** POLYNOMES CREUX ********************************/

p_polyf_creux_t creer_polynome_creux (int nb_terme) {
  p_polyf_creux_t p ;
  
  p = (p_polyf_creux_t) malloc (sizeof (polyf_creux_t)) ;

  p->nb_terme = nb_terme;
  p->expo = (int *) malloc(nb_terme * sizeof (int));
  p->coeff = (float *) malloc(nb_terme * sizeof (float));

  return p ;
}

void detruire_polynome_creux (p_polyf_creux_t p) {
  free(p->expo);
  free(p->coeff);
  free(p);
}

void init_polynome_creux (p_polyf_creux_t p, float x) {

  for (int i = 0 ; i <= p->nb_terme; ++i) {
    p->expo[i]=i;
    p->coeff[i] = x;
  }
}

p_polyf_creux_t lire_polynome_creux_float (char *nom_fichier) {
  FILE *f ;
  p_polyf_creux_t p ;
  int cr, i, nb_terme;
  
  f = fopen (nom_fichier, "r") ;
  if (f == NULL) {
    fprintf (stderr, "erreur ouverture %s \n", nom_fichier) ;
    exit (-1) ;
  }
  
  cr = fscanf (f, "%d", &nb_terme) ;
  if (cr != 1) {
    fprintf (stderr, "erreur lecture du degre\n") ;
    exit (-1) ;
  }
  p = creer_polynome_creux (nb_terme) ;
  
  for (i = 0 ; i < nb_terme; i++) { 
    cr = fscanf (f, "%d", &p->expo[i]) ;
    if (cr != 1) {
      fprintf (stderr, "erreur lecture exposant %d\n", i) ;
      exit (-1) ;
    }

    cr = fscanf (f, "%f", &p->coeff[i]) ;
    if (cr != 1) {
      fprintf (stderr, "erreur lecture coefficient %d\n", i) ;
      exit (-1) ;
    }
  }
  fclose (f) ;
  return p ;
}

void ecrire_polynome_creux_float (p_polyf_creux_t p) {
  int i = 0 ;
  if(p->expo[i] == 0) {
    printf ("%f", p->coeff [0]) ;
    i = 1;
  }else{
     printf ("%f X^%d ", p->coeff[i], p->expo[i]);
  }
  for (i=1 ; i < p->nb_terme; i++) {  
    if(p->coeff[i] != 0)
      printf ("+ %f X^%d ", p->coeff[i], p->expo[i]);
  }
  printf ("\n") ;
}

int egalite_polynome_creux (p_polyf_creux_t p1, p_polyf_creux_t p2) {
  /*  Vérifier que les nombres de termes sont identiques   */
  if(p1->nb_terme != p2->nb_terme)
    return 0;

  /*  Vérifier que tous les coefficients sont identiques   */
  for(int i = 0; i <= p1->nb_terme; i++) {
    if(p1->coeff[i] != p2->coeff[i] || p1->expo[i] != p2->expo[i] )
      return 0;
  }

  /*  Si les vérifications sont arrivées jusqu'ici, 
   *  alors les deux polynomes sont égaux.   
   */
  return 1;
}

p_polyf_creux_t addition_polynome_creux (p_polyf_creux_t p1, p_polyf_creux_t p2) {
  p_polyf_creux_t p3 ;
  int present = 0;
  int nb_poly_diff = p1->nb_terme;  // car nous comparons p2 à partir de p1

  for (int i = 0; i < p2->nb_terme; i++) {
    present = 0;
    for(int j = 0; j < p1->nb_terme; j++) {
      if (p1->expo[j] == p2->expo[i])   // verifie si l'exposant de p1 est présent dans p2
        present = 1;
    }
    if (present == 0) // si l'exposant n'est pas trouvé dans p2, 
      nb_poly_diff++; // on incrémente de compteur de différences
  }

  p3 = creer_polynome_creux(nb_poly_diff);

  int c1 = 0;      // pour p1
  int c2 = 0;      // pour p2
  for(int k = 0; k < nb_poly_diff; k++) {
    if(c2 >= p2->nb_terme || (c1 < p1->nb_terme && p1->expo[c1] < p2->expo[c2])) {
      p3->expo[k] = p1->expo[c1];
      p3->coeff[k] = p1->coeff[c1];
      c1++;
    }else if(c1 >= p1->nb_terme || (c2 < p2->nb_terme && p2->expo[c2] < p1->expo[c1])) {
      p3->expo[k] = p2->expo[c2];
      p3->coeff[k] = p2->coeff[c2];
      c2++;
    }else{         //(p2->expo[c2] == p1->expo[c1])
      p3->expo[k] = p2->expo[c2];
      p3->coeff[k] = p1->coeff[c1] + p2->coeff[c2];
      c2++;
      c1++;
    }
  }
  return p3 ;
}

p_polyf_creux_t addition_polynome_creuxV2 (p_polyf_creux_t p1, p_polyf_creux_t p2) {
  p_polyf_creux_t p3 ;
  int nb_poly_diff =0;  // car nous comparons p2 à partir de p1
  for(int i=0;i<p1->nb_terme;i++) {
    if(p1->coeff[i]!=0) {
      nb_poly_diff++;
    }
  }
  for(int i=0;i<p2->nb_terme;i++) {
    if(p2->coeff[i]!=0) {
      nb_poly_diff++;
    }
  } //prend tout les coef différent de zéro
  for (int i = 0; i < p2->nb_terme; i++) {
   for(int j=0; j<p1->nb_terme;j++) {
     if (p1->expo[j] == p2->expo[i]) {
       nb_poly_diff --;
       if(p1->coeff[j] + p2->coeff[i]==0) {
        nb_poly_diff--;
       }
     }
   }
  }

  p3 = creer_polynome_creux(nb_poly_diff);

  int c1 = 0;      // pour p1
  int c2 = 0;      // pour p2
  int k=0;
  while(k < nb_poly_diff) {
    if(c2 >= p2->nb_terme || (c1 < p1->nb_terme && p1->expo[c1] < p2->expo[c2])) {
      if(p1->coeff[c1]!=0) {
      p3->expo[k] = p1->expo[c1];
      p3->coeff[k] = p1->coeff[c1];
      k++;
      }
      c1++;
    }else if(c1 >= p1->nb_terme || (c2 < p2->nb_terme && p2->expo[c2] < p1->expo[c1])) {
      if(p2->coeff[c2]!=0) {
      p3->expo[k] = p2->expo[c2];
      p3->coeff[k] = p2->coeff[c2];
      k++;
      }
      c2++;
    }else{         //(p2->expo[c2] == p1->expo[c1])
      if(p1->coeff[c1]+p2->coeff[c2]!=0) {
      p3->expo[k] = p2->expo[c2];
      p3->coeff[k] = p1->coeff[c1] + p2->coeff[c2];
      k++;
      }
      c2++;
      c1++;
    }
  }
  return p3 ;
}

p_polyf_creux_t multiplication_polynome_creux_scalaire (p_polyf_creux_t p, float alpha) {
   p_polyf_creux_t result = creer_polynome_creux(p->nb_terme);
    for(int i=0;i<p->nb_terme;i++) {
      result->coeff[i]=alpha*p->coeff[i];
      result->expo[i]=p->expo[i];
    }
  /*}else{
    result=creer_polynome(p->nb_terme);
    init_polynome(result,0.0);
  }*/
  return result;
}


float eval_polynome_creux (p_polyf_creux_t p, float x) {
  float result=0;
  for (int i = 0; i <= p->nb_terme; i++) {
    result += p->coeff[i]*puissance(x,p->expo[i]);
  }
  return result;
}

p_polyf_creux_t multiplication_polynomes_creux (p_polyf_creux_t p1, p_polyf_creux_t p2) {
  p_polyf_creux_t result = creer_polynome_creux(0);
  init_polynome_creux(result,0.0);
  for(int i = 0; i<p1->nb_terme; i++) {
    
    p_polyf_creux_t temp=creer_polynome_creux(p2->nb_terme);
    for (int j=0;j<temp->nb_terme;j++) {
     
      temp->coeff[j]=p2->coeff[j];
      temp->expo[j]=p2->expo[j]+p1->expo[i];

    }
    temp = multiplication_polynome_creux_scalaire(temp,p1->coeff[i]);
    detruire_polynome_creux(temp);
    //printf("%d\n",i);
    result = addition_polynome_creuxV2(result,temp);
    //printf("fin\n\n");
  }
  return result;
}

p_polyf_creux_t puissance_polynome_creux (p_polyf_creux_t p, int n) {
  p_polyf_creux_t temp = creer_polynome_creux(1);
  init_polynome_creux(temp,1.0);
  for (int i = 1; i <= n; i++) {
    temp = multiplication_polynomes_creux(temp,p);
  }
  return temp;
}

p_polyf_creux_t composition_polynome_creux (p_polyf_creux_t p, p_polyf_creux_t q) {
  
  /*  p O q  */
  p_polyf_creux_t result = creer_polynome_creux(q->nb_terme);
  p_polyf_creux_t temp = creer_polynome_creux(q->nb_terme);
  p_polyf_creux_t puissance = creer_polynome_creux(q->nb_terme);
  init_polynome_creux(result,0);

  for(int i = 0;i < p->nb_terme; i++) {
    puissance = puissance_polynome_creux(q,p->expo[i]);
    temp = multiplication_polynome_creux_scalaire(puissance,p->coeff[i]);
    result = addition_polynome_creux(result,temp);
    detruire_polynome_creux(temp);
    detruire_polynome_creux(puissance);
  }
  return result ;
}
