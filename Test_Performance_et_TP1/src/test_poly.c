#include <stdio.h>
#include <stdlib.h>

#include "../lib/poly.h"

int main (int argc, char **argv) {
  p_polyf_t p1, p2, p_res;
  
  if (argc != 3) {
    fprintf (stderr, "deux paramètres (polynomes,fichiers) sont à passer \n") ;
    exit (-1) ;
  }
      
  p1 = lire_polynome_float (argv [1]) ;
  p2 = lire_polynome_float (argv [2]) ;

  ecrire_polynome_float (p1) ;
  ecrire_polynome_float (p2) ;

  /*
    Test des fonctions sur les polynomes
  */

  if(egalite_polynome(p1, p2))
    printf("p1 et p2 sont égaux\n");
  if(egalite_polynome(p1, p1))
    printf("p1 est égal à lui-même\n");
  if(egalite_polynome(p2, p2))
    printf("p2 est égal à lui-même\n");
  printf("\n");
  
  p_res = addition_polynome(p1, p2);
  printf("addition_polynome: \n");
  ecrire_polynome_float (p_res);
  printf("\n");
  detruire_polynome(p_res);

  p_res = multiplication_polynome_scalaire(p1, 2.0);
  printf("multiplication_polynome_scalaire avec 2.0: \n");
  ecrire_polynome_float (p_res);
  printf("\n");
  detruire_polynome(p_res);

  float tmp = eval_polynome(p1, 2.5);
  printf("eval_polynome avec 2.5: %f\n", tmp);
  printf("\n");

  p_res = multiplication_polynomes(p1, p2);
  printf("multiplication_polynomes: \n");
  ecrire_polynome_float (p_res);
  printf("\n");
  detruire_polynome(p_res);

  p_res = puissance_polynome(p1, 3);
  printf("puissance_polynome de p1 à la puissance 3: \n");
  ecrire_polynome_float (p_res);
  printf("\n");
  detruire_polynome(p_res);

  p_res = composition_polynome(p1, p2);
  printf("composition_polynome: \n");
  ecrire_polynome_float (p_res);

  detruire_polynome(p_res);
  detruire_polynome(p1);
  detruire_polynome(p2);
}
