#include <stdio.h>
#include <nmmintrin.h>

typedef float float4 [4]  __attribute__ ((aligned (16))) ;
typedef double double2 [2]  __attribute__ ((aligned (16))) ;
typedef short int int8 [8]   __attribute__ ((aligned (16))) ;

void print_vector_float (float4 T) ;
void print_vector_double (double2 T) ;
void print_vector_int (int8 T) ;

int main (int argc, char **argv) {
  float4 a = {1.0f, 2.0f, 3.0f, 4.0f} ;
  float4 b = {1.0f, 2.0f, 3.0f, 5.0f} ;
  float4 c ;

  double2 A = {2.0d, 4.0d} ;
  double2 B = {5.0d, 5.0d} ;
  double2 C ;

  __m128 v1, v2, v3 ;
  __m128d V1, V2, V3 ;

  v1 = _mm_load_ps (a) ;
  v2 = _mm_load_ps (b) ;
  v3 = _mm_add_ps (v1, v2) ;
  _mm_store_ps (c, v3) ;
  print_vector_float (c) ;

  V1 = _mm_load_pd (A) ;
  V2 = _mm_load_pd (B) ;
  V3 = _mm_add_pd (V1, V2) ;
  _mm_store_pd (C, V3) ;
  print_vector_double (C) ;

  exit (0) ;
}


void print_vector_float (float4 T) {
  printf ("float : %f %f %f %f\n", T[0], T[1], T[2],  T[3]) ;
  return ;
}

void print_vector_double (double2 T) {
  printf ("double : %lf %lf\n", T[0], T[1]) ;
  return ;
}

