#include <stdio.h>

#include <xmmintrin.h>

int main (int argc, char**argv) {
  float T1 [4] __attribute__ ((aligned (16))) 
    = {1.4, 2.5, 3.6, 4.8} ;

  float T2 [4] __attribute__ ((aligned (16))) ;

  __m128 v1, v2 ;

  v1 = _mm_load_ps (T1) ;

  _mm_store_ps (T2, v1) ;

  printf (" %f %f %f %f\n", T2[0],  T2[1],  T2[2],  T2[3]) ;

  return 0 ;
}
