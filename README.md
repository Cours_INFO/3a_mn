# Tous les Documents - Méthodes Numériques

Tous les travaux pratiques dans le cadre du cours de Méthodes Numériques à **POLYTECH GRENOBLE**.

***

## **TABLE DES MATIÈRES**

* **[Test performance](./Test_Performance_et_TP1/README.md)**

* **[TP1 - Polynômes Pleins et Creux](./Test_Performance_et_TP1/README.md)**

* **[TP2 - BLAS (Bibliothèque d’Algèbre Linéaire) et OpenMP](./TP2/README.md)**

***
